<?php

/**
 * @file
 * GPA Calculator.
 *
 * @ingroup hfcc_modules
 */

/**
 * Implements hook_menu().
 */
function gpacalc_menu() {
  $items['calc-gpa'] = array(
    'title' => 'Estimated GPA Calculator',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('gpacalc_form'),
    'access callback' => TRUE,
    );
  return $items;
}

/**
 * GPA Calculation Form
 */
function gpacalc_form($form, &$form_state) {
  drupal_add_js(drupal_get_path('module', 'gpacalc') .'/js/gpa_calc.js');
  drupal_add_css(drupal_get_path('module', 'gpacalc') .'/css/gpa_calc.css');

  $form_title = t('Calculate Estimated GPA');

  $form = array();

  $form['semester'] = array(
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
  );

  for ($coursenumber=1; $coursenumber < 9; $coursenumber++) {
    $courserow = 'course' . $coursenumber;
    $creditfield = 'credithours' . $coursenumber;
    $gradefield = 'grade' . $coursenumber;
    $form['semester'][$courserow] = array(
      '#prefix' => '<div style="display: flex">',
      '#suffix' => '</div>',
      '#weight' => $coursenumber,
    );
    $form['semester'][$courserow][$creditfield] = array(
      '#title' => t('Credits'),
      '#type' => 'textfield',
      '#size' => 5,
      '#attributes' => array(
        'id' => $creditfield,
        'class' => array('semester-hours'),
      ),
      '#weight' => 1,
    );
    $form['semester'][$courserow][$gradefield] = array(
      '#title' => t('Grade'),
      '#type' => 'select',
      '#options' => gpacalc_grade_opts(),
      '#attributes' => array(
        'id' => $gradefield,
        'class' => array('semester-grade'),
      ),
      '#weight' => 2,
    );
  }

  $form['previous_credits'] = array(
    '#title' => t('Previous Total Credit Hours'),
    '#type' => 'textfield',
    '#size' => 5,
    '#attributes' => array(
      'id' => 'prev-credits',
    ),
  );

  $form['previous_gpa'] = array(
    '#title' => t('Previous Cumulative GPA'),
    '#type' => 'textfield',
    '#size' => 5,
    '#attributes' => array(
      'id' => 'prev-gpa',
    ),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Calculate Estimated GPA'),
    '#executes_submit_callback' => FALSE,
    '#weight' => 100,
  );

  $form['output'] = array(
    '#prefix' => '<div id="gpa-info">',
    '#suffix' => '</div>',
    '#weight' => 101,
  );

  $instructions = <<<'EOT'

  <h3>How to calculate Grade Point Average (Estimated GPA) using the GPA Calculator</h3>
  <p>Enter your graded credit hours <strong>for this semester</strong> and select the letter grade earned in <strong>each class for this semester</strong>. When you click the calculate button, your Estimated GPA will be displayed, based on your grade, honor points, and credit hours attempted.</p>

  <h3>Formula to Calculate Grade Point Average (Estimated GPA)</h3>
  <p>First, calculate total honor points for each course by multiplying the <strong>credit hours for each course</strong> by the number of <strong>honor points (see below)</strong> appropriate for the grade you received in that course. Add the honor point totals for each course to find the <strong>sum of honor points</strong> for the current semester. Use the <strong>sum of honor points</strong> in the formula below.</p>
  <p>Add the total number of credits you are taking this semester and use the Sum of Credit Hours in the formula below.</p>

  <h4>Following are Honor points per semester hour of credit:</h4>
  <ul>
  <li>A+ = 4 grade points</li>
  <li>A = 4 grade points</li>
  <li>A- = 3.67 grade points</li>
  <li>B+ = 3.33 grade points</li>
  <li>B = 3 grade points</li>
  <li>B- = 2.67 grade points</li>
  <li>C+ = 2.33 grade points</li>
  <li>C = 2 grade points</li>
  <li>C- = 1.67 grade points</li>
  <li>D+ = 1.33 grade point</li>
  <li>D = 1 grade point</li>
  <li>D- = 0.67 grade point</li>
  <li>E = 0 grade points</li>
  </ul>
  <p>Grade Point Average (Estimated GPA) = Sum of Honor Points (see above) for the Current Semester / Sum of Credit Hours Attempted for the Current Semester</p>

  <h3>Example</h3>
  <p>Your Grade Point Average (Estimated GPA) is a weighted average of all your course work. This means that a grade in a four-hour course will change your estimated GPA more than the same grade in a three-hour course. It also means that your estimated GPA gets harder to change when you have taken more courses. To calculate your estimated GPA, you would do the following:</p>
  <p>For example, suppose you were taking the following courses:</p>

  <table>
  <tr>
  <th width="25%">Course</th>
  <th>Credit Hours</th>
  <th>Letter Grade</th>
  <th>Honor Points</th>
  <th width="35%">Total Honor Points (Credit Hours x Honor Point for Letter Grade)</th>
  </tr>
  <tr>
  <td>Course 1</td>
  <td>3</td>
  <td>A</td>
  <td>4</td>
  <td>12</td>
  </tr>
  <tr>
  <td>Course 2 </td>
  <td>5</td>
  <td>C</td>
  <td>2</td>
  <td>10</td>
  </tr>
  <tr>
  <td>Course 3 </td>
  <td>2</td>
  <td>B</td>
  <td>3</td>
  <td>6</td>
  </tr>
  <tr>
  <td>Course 4 </td>
  <td>3</td>
  <td>D</td>
  <td>1</td>
  <td>3</td>
  </tr>
  <tr>
  <td><strong>Total</strong></td>
  <td><strong>13</strong></td>
  <td colspan="2">&nbsp;</td>
  <td><strong>31</strong></td>
  </tr>
  </table>

  <p>Your grade point total for the semester (31) is divided by your total credit hours (13) which equals your estimated GPA (2.38).</p>

  <h3>How to calculate Cumulative Grade Point Average (Estimated GPA)</h3>

  <p>Enter credit hours and grades you earned during the current semester into the GPA Calculator above. Then enter your cumulative GPA <strong>prior to</strong> this semester into the "Previous Cumulative GPA" field, as well as the total number of <strong>graded credit hours</strong> earned <strong>prior to</strong> this semester into the "Previous Total Credit Hours" field on the Cumulative GPA Calculator. When you click the calculate button, your new cumulative estimated GPA will be displayed, based on your previous GPA and this semesters estimated GPA (calculated on the GPA Calculator above).</p>

  <p><strong><em>* GPA Calculator estimates what your current GPA may be based on your input, but is not necessarily your actual GPA depending on variations in calculations, timing, etc. </em></strong></p>

  <h3>Formula to Cumulative Grade Point Average (Estimated GPA)</h3>

  <p>Cumulative Grade Point Average = Sum of Honor Points Earned <strong>All Semesters</strong> / Sum of Credit Hours Attempted <strong>All Semesters</strong></p>

EOT;

  $form['instructions'] = array(
    '#markup' => $instructions,
    '#weight' => 102,
  );

  return $form;
}

function gpacalc_grade_opts($option = NULL) {
  $options = array(
    '-1' => t('---'),
    '4.00' => t('A+'),
    '4.0' => t('A'),
    '3.67' => t('A-'),
    '3.33' => t('B+'),
    '3.0' => t('B'),
    '2.67' => t('B-'),
    '2.33' => t('C+'),
    '2.0' => t('C'),
    '1.67' => t('C-'),
    '1.33' => t('D+'),
    '1.0' => t('D'),
    '0.67' => t('D-'),
    '0.0' => t('E'),
    '-2' => t('I'),
    '-3' => t('DR'),
    '-4' => t('W'),
    '-5' => t('S'),
    '-6' => t('U'),
  );
  return $options;
}

?>
