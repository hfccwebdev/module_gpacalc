(function ($) {

  $(document).ready(function() {

    $('#gpacalc-form').submit(function() {

      var totalpoints = 0;
      var totalcredithours = 0;

      for (coursenumber = 1; coursenumber <= 8; coursenumber++) {
        var gradenumber = '#grade' + coursenumber;
        var creditnumber = '#credithours' + coursenumber;
        var grade = $(gradenumber).val();
        var credits = isNaN(parseInt($(creditnumber).val())) ? 0 : parseInt($(creditnumber).val());

        if (grade > -1 && credits > 0) {
          totalpoints += (grade * credits);
          totalcredithours += credits;
        }
      }

      if (totalcredithours > 0) {
        var gparesult = (Math.round(1000 * totalpoints / totalcredithours) / 1000).toFixed(3);
      } else {
        var gparesult = 0;
        gparesult = gparesult.toFixed(3);
      }

      var previous_credits = isNaN(parseInt($('#prev-credits').val())) ? 0 : parseInt($('#prev-credits').val());
      var previous_gpa = isNaN(parseFloat($('#prev-gpa').val())) ? 0 : parseFloat($('#prev-gpa').val()).toFixed(3);

      console.log(previous_credits);
      console.log(previous_gpa);

      if (previous_credits > 0 && previous_gpa > 0) {
        var previous_points = (previous_credits * previous_gpa);
        var lifetime_points = (previous_points + totalpoints);
        var lifetime_credits = (previous_credits + totalcredithours);
        var lifetime_gpa = (Math.round(1000 * lifetime_points /lifetime_credits) /1000).toFixed(3);
      }

      var output = '<h3>Estimated GPA</h3>';
      output += '<p>Your estimated GPA for this semester is ' + gparesult + '.</p>';
      if (lifetime_gpa) {
        output += '<p>Your estimated cumulative GPA is ' + lifetime_gpa + '.</p>';
      }
      $('#gpa-info').html(output);

      return false;

    });
  });
})(jQuery);
